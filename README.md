# Supplementary materials

## CFD model of slot die coating for lithium-Ion battery electrodes in 2D and 3D with load balanced dynamic mesh refinement enabled with a local-slip boundary condition in OpenFOAM

In this repository, supplementary materials for the mesh generation and simulations are provided. This includes

- OpenFOAM cases in 2D and 3D ready for simulation
- OpenSCAD files for generating the slot die geometry
- localSlipSlotDieCoating boundary condition source code

The load-balanced algorithm for dynamic mesh refinement in 2D and 3D is located here: 

https://github.com/HenningScheufler/multiDimAMR