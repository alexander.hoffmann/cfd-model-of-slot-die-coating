//___________________ABMESSUNGEN___________________
//---------------DÜSE
    duese_schlitzweite = 0.0005;
    duese_breite_innen= 0.012; //gesamter innenraum, wird später halbiert für Symmetrie
    duese_hoehe = 0.003;
    duese_wandstaerke = 0.001;
    duese_wandstaerke_seite = 0.001;
    duese_upstream_lippe_laenge = duese_wandstaerke;
    duese_downstream_lippe_laenge = duese_wandstaerke;
    duese_breite = duese_breite_innen + 2*duese_wandstaerke_seite;
    downstream_auslauf_laenge = 0.01;   
 
    //SHIM
    shim_fase_laenge = 0.000;
    shim_fase_breite = -0.0000;

    //SPALT
    spalt_hoehe = 0.000180;

//-------------NASSFILMHÖHE
    substratgeschwindigkeit = 0.0833; //m/s
    nassfilmhoehe = 0.000156;
    inlet_flaeche = duese_schlitzweite*duese_breite_innen;
    substrat_volumenstrom = duese_breite_innen*nassfilmhoehe*substratgeschwindigkeit;
    inlet_geschwindigkeit = substrat_volumenstrom/inlet_flaeche;

//--------------RANDBEREICHE
    upstream_puffer_laenge = 0.0005;
    upstream_puffer_hoehe = 0.0004;
    downstream_puffer_laenge = 0.0005;
    downstream_puffer_hoehe = upstream_puffer_hoehe;

    seite_puffer_breite = 0.001;


//--------------VERFEINERUNG
    ausdehnung_upstream =                   0.0009;
    ausdehnung_downstream =                 0.0007;
    refinement_box_hoehe_phasengrenze =     nassfilmhoehe/2.2;
    refinement_box_hoehe_randbereich =      nassfilmhoehe+refinement_box_hoehe_phasengrenze/2;
    refinement_box_breite_randbereich =     duese_wandstaerke*2;
    

//--------------MAIN
    blockMeshEntries();     //vertices / blocks 
    snappyHexMeshEntries(); //geometric boxes verfeinerung
    setFieldsEntries();
    U_rb();

    //GRUPPEN NACH RANDBEDINGUNG UND REFINEMENT
    //--------------------methoden nacheinander auskommentieren und einzeln als gleichnamige .stl abspeichern-------------
        //wall();
        //movingWallRef();
        //movingWallUpstream();
        //movingWallDownstream();
        //movingWallSide();
        //wallDie();
        //inlet();
        //outlet();
        //outletDownstream();
        symmetrie();

//-------------GRUPPEN
module wall() {
    wand_duese_innen();
    wand_duese_aussen();
}

module movingWallRef() {
    substrat_duese();
}

module movingWallUpstream() {
    substrat_upstream();
}

module movingWallDownstream() {
    substrat_downstream();    
}

module movingWallSide() {
    substrat_side();    
}

module wallDie() {
    upstream_lippe();
    downstream_lippe();
    seite_lippe();
}

module inlet() {
    inlet_top();
}

module outlet() {
    outlet_top();
    outlet_side();
    outlet_upstream();
}

module outletDownstream() {
    outlet_downstream();
}



//-------------MODULES
module U_rb () {
    echo("-------------U Randbedingung-------------");
    echo("inlet");
    echo("{");
    echo("type		fixedValue;");
    echo("value		uniform (0 0 ",-inlet_geschwindigkeit,");");
    echo("}");
}
module blockMeshEntries() {
    echo("-------------blockmesh-------------");
    echo("vertices");
    echo("(");
    echo("(",-(duese_upstream_lippe_laenge+upstream_puffer_laenge + 0.00001), 0, 0, ")", "//0"
    );
    echo("(",
    ((duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge)+ 0.00001, 0, 0, ")", "//1"
    );
    echo("(",((duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge)+ 0.00001, (duese_breite/2 + seite_puffer_breite)+ 0.00001, 0, ")", "//2"
    );
    echo("(",-(duese_upstream_lippe_laenge+upstream_puffer_laenge + 0.00001), (duese_breite/2 + seite_puffer_breite)+ 0.00001, 0, ")", "//3"
    );
    echo("(",-(duese_upstream_lippe_laenge+upstream_puffer_laenge + 0.00001), 0, (spalt_hoehe+duese_hoehe)+ 0.00001, ")", "//4"
    );
    echo("(",((duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge)+ 0.00001, 0, (spalt_hoehe+duese_hoehe)+ 0.00001, ")", "//5"
    );
    echo("(",((duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge)+ 0.00001, (duese_breite/2 + seite_puffer_breite)+ 0.00001, (spalt_hoehe+duese_hoehe)+ 0.00001, ")", "//6"
    );
    echo("(",-(duese_upstream_lippe_laenge+upstream_puffer_laenge + 0.00001), (duese_breite/2 + seite_puffer_breite)+ 0.00001, (spalt_hoehe+duese_hoehe)+ 0.00001, ")", "//7"
    );
    echo(");");
    echo("blocks");
    echo("(");
    echo("(0 1 2 3 4 5 6 7) (10 10 10) simpleGrading (1 1 1)");
    echo(");");
    echo("-------------end blockmesh-------------");
}


module snappyHexMeshEntries() {
    echo("-------------snappyHexMesh-------------");
    echo("//geometry");    
    echo("phasengrenze");
    echo("{");
    echo("type searchableBox;");
    echo("min (",duese_schlitzweite+duese_downstream_lippe_laenge,"0",nassfilmhoehe-refinement_box_hoehe_phasengrenze*2/3,");");
    echo("max (",(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,duese_breite_innen/2,nassfilmhoehe+refinement_box_hoehe_phasengrenze/3,");");
    echo("}");
    echo("");   
    echo("randbereich");
    echo("{");
    echo("type searchableBox;");
    echo("min (",duese_schlitzweite+duese_downstream_lippe_laenge/3,duese_breite_innen/2-refinement_box_breite_randbereich/2,0,");");
    echo("max (",(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,duese_breite_innen/2 + refinement_box_breite_randbereich/2,refinement_box_hoehe_randbereich,");");
    echo("}");
    echo("");  
    
    echo("//refinement Regions");
    echo("phasengrenze");
    echo("{");
    echo("mode inside;");
    echo("levels ((0 1));"); 
    echo("} ");
    echo("");  
    echo("randbereich");
    echo("{");
    echo("mode inside;");
    echo("levels ((0 1));"); 
    echo("} ");
    echo("-------------end snappyHexMesh-------------");
    echo("");
}

module setFieldsEntries() {
    echo("-------------setFieldsDict: Box for alpha=1 initial value-------------");
    echo("box (0 0 ",spalt_hoehe,") (",duese_schlitzweite,duese_breite_innen/2,spalt_hoehe+duese_hoehe,");"); 
    echo("-------------end setFieldsDict-------------"); 
}
    
   
module wand_duese_innen() {
    color("Aqua"){
    //Wand upstream
    polyhedron(
    [[0,0,spalt_hoehe],
    [0,0,spalt_hoehe+duese_hoehe],
    [0,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [0,duese_breite_innen/2,spalt_hoehe+shim_fase_laenge],
    [0,duese_breite_innen/2,spalt_hoehe+duese_hoehe]],
    [[0,1,4,3,2]]
    );
    
    //Wand downstream
    polyhedron(
    [[duese_schlitzweite,0,spalt_hoehe],
    [duese_schlitzweite,0,spalt_hoehe+duese_hoehe],
    [duese_schlitzweite,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [duese_schlitzweite,duese_breite_innen/2,spalt_hoehe+shim_fase_laenge],
    [duese_schlitzweite,duese_breite_innen/2,spalt_hoehe+duese_hoehe]],
    [[0,1,4,3,2]]
    );
    
    //Wand seite
    polyhedron(
    [[0,                    duese_breite_innen/2, spalt_hoehe+shim_fase_laenge],
    [0,                     duese_breite_innen/2, spalt_hoehe+duese_hoehe],
    [duese_schlitzweite,    duese_breite_innen/2, spalt_hoehe+shim_fase_laenge],
    [duese_schlitzweite,    duese_breite_innen/2, spalt_hoehe+duese_hoehe]],
    [[0,1,3,2]]
    );
    
    //Wand shim
    polyhedron(
    [[duese_schlitzweite,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [duese_schlitzweite,duese_breite_innen/2,spalt_hoehe+shim_fase_laenge],
    [0,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [0,duese_breite_innen/2,spalt_hoehe+shim_fase_laenge],],
    [[0,1,3,2]]
    );
}}

module wand_duese_aussen() {
    color("DodgerBlue"){
    polyhedron(
    [[-duese_upstream_lippe_laenge,0,spalt_hoehe],
    [-duese_upstream_lippe_laenge,duese_breite/2,spalt_hoehe],
    [-duese_upstream_lippe_laenge,0,spalt_hoehe + upstream_puffer_hoehe], 
    [-duese_upstream_lippe_laenge,duese_breite/2,spalt_hoehe + upstream_puffer_hoehe]],
    [[0,2,3,1]]
    ); 
    
    polyhedron(
    [[duese_schlitzweite + duese_downstream_lippe_laenge,0,spalt_hoehe],
    [duese_schlitzweite + duese_downstream_lippe_laenge,duese_breite/2,spalt_hoehe],
    [duese_schlitzweite + duese_downstream_lippe_laenge,0,spalt_hoehe + downstream_puffer_hoehe], 
    [duese_schlitzweite + duese_downstream_lippe_laenge,duese_breite/2,spalt_hoehe + downstream_puffer_hoehe]],
    [[0,2,3,1]]
    );
        
    polyhedron(
    [[-duese_upstream_lippe_laenge,duese_breite/2,spalt_hoehe],
    [duese_downstream_lippe_laenge+duese_schlitzweite,duese_breite/2,spalt_hoehe],
    [-duese_upstream_lippe_laenge,duese_breite/2,spalt_hoehe + upstream_puffer_hoehe], 
    [duese_downstream_lippe_laenge+duese_schlitzweite,duese_breite/2,spalt_hoehe + upstream_puffer_hoehe]],
    [[0,1,3,2]]
    );
}}

module upstream_lippe() {
    color("SteelBlue"){
    polyhedron(
    [[0,0,spalt_hoehe],
    [0,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [-duese_upstream_lippe_laenge,duese_breite_innen/2-shim_fase_breite,spalt_hoehe], 
    [-duese_upstream_lippe_laenge,0,spalt_hoehe]],
    [[0,1,2,3]]
    );
}}

module downstream_lippe() {
    color("DodgerBlue"){
    polyhedron(
    [[duese_schlitzweite,0,spalt_hoehe],
    [duese_schlitzweite,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [duese_downstream_lippe_laenge+duese_schlitzweite,duese_breite_innen/2-shim_fase_breite,spalt_hoehe], 
    [duese_downstream_lippe_laenge+duese_schlitzweite,0,spalt_hoehe]],
    [[0,1,2,3]]
    );
}}

module seite_lippe() {
    color("skyblue"){
    polyhedron(
    [[-duese_upstream_lippe_laenge,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [duese_downstream_lippe_laenge+duese_schlitzweite,duese_breite_innen/2-shim_fase_breite,spalt_hoehe],
    [-duese_upstream_lippe_laenge,duese_breite/2,spalt_hoehe], 
    [duese_downstream_lippe_laenge+duese_schlitzweite,duese_breite/2,spalt_hoehe]],
    [[0,1,3,2]]
    );
}}

module substrat_upstream() {
    color("Brown")
    polyhedron(
    [[-ausdehnung_upstream,0,0],
    [-ausdehnung_upstream,duese_breite/2,0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),duese_breite/2,0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),0,0]],
    [[0,1,2,3]]
    );
}

module substrat_duese() {
    color("Brown")
    polyhedron(
    [[-ausdehnung_upstream,0,0],
    [-ausdehnung_upstream,duese_breite/2,0],
    [(duese_schlitzweite+ausdehnung_upstream),duese_breite/2,0],
    [(duese_schlitzweite+ausdehnung_upstream),0,0]],
    [[0,1,2,3]]
    );
}

module substrat_downstream() {
    color("Brown")
    polyhedron(
    [[(duese_schlitzweite+ausdehnung_downstream),duese_breite/2,0],
    [(duese_schlitzweite+ausdehnung_downstream),0,0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,0,0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,duese_breite/2,0]],
    [[0,1,2,3]]
    );
}

module substrat_side() {
    color("Brown")
    polyhedron(
    [[-(duese_upstream_lippe_laenge+upstream_puffer_laenge),duese_breite/2,0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),duese_breite/2 + seite_puffer_breite,0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,duese_breite/2,0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,duese_breite/2 + seite_puffer_breite,0]],
    [[1,0,2,3]]
    );
}

module inlet_top() {
    color("Lime")
    polyhedron(
    [[0,0,spalt_hoehe+duese_hoehe],
    [duese_schlitzweite,0,spalt_hoehe+duese_hoehe],
    [duese_schlitzweite,duese_breite_innen/2,spalt_hoehe+duese_hoehe],
    [0,duese_breite_innen/2,spalt_hoehe+duese_hoehe]],

    [[0,1,2,3]]
    );   
}

module outlet_top() {
    //upstream
    polyhedron(
    [[-duese_upstream_lippe_laenge, duese_breite/2 + seite_puffer_breite, spalt_hoehe+upstream_puffer_hoehe],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),duese_breite/2 + seite_puffer_breite, spalt_hoehe+upstream_puffer_hoehe],
    [-duese_upstream_lippe_laenge, 0, spalt_hoehe+upstream_puffer_hoehe],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),   0, spalt_hoehe+upstream_puffer_hoehe]],
    [[0,1,3,2]]
    );
    
    //seite
    polyhedron(
    [[-duese_upstream_lippe_laenge, duese_breite/2 + seite_puffer_breite, spalt_hoehe+upstream_puffer_hoehe],
    [-duese_upstream_lippe_laenge, duese_breite/2, spalt_hoehe+upstream_puffer_hoehe],
    [duese_downstream_lippe_laenge + duese_schlitzweite, duese_breite/2 + seite_puffer_breite, spalt_hoehe+upstream_puffer_hoehe],
    [duese_downstream_lippe_laenge + duese_schlitzweite, duese_breite/2, spalt_hoehe+upstream_puffer_hoehe]],
    [[0,1,3,2]]
    ); 

    //downstream
    polyhedron(
    [[duese_downstream_lippe_laenge + duese_schlitzweite, 0, spalt_hoehe + downstream_puffer_hoehe],
    [duese_schlitzweite + ausdehnung_downstream + downstream_auslauf_laenge, 0, spalt_hoehe + downstream_puffer_hoehe],
    [duese_downstream_lippe_laenge + duese_schlitzweite, duese_breite/2 + seite_puffer_breite, spalt_hoehe+upstream_puffer_hoehe],
    [duese_schlitzweite + ausdehnung_downstream + downstream_auslauf_laenge, duese_breite/2 + seite_puffer_breite, spalt_hoehe+upstream_puffer_hoehe]],
    [[0,1,3,2]]
    );    
}

module outlet_side() {
    color("Grey"){
    polyhedron(
    [[-(duese_upstream_lippe_laenge+upstream_puffer_laenge), duese_breite/2 + seite_puffer_breite, 0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge), duese_breite/2 + seite_puffer_breite, spalt_hoehe + upstream_puffer_hoehe],
    [duese_schlitzweite + ausdehnung_downstream + downstream_auslauf_laenge, duese_breite/2 + seite_puffer_breite, 0],
    [duese_schlitzweite + ausdehnung_downstream + downstream_auslauf_laenge, duese_breite/2 + seite_puffer_breite, spalt_hoehe + upstream_puffer_hoehe]],
    [[0,1,3,2]]
    );   
}}

module outlet_upstream() {
    color("Grey"){
    polyhedron(
    [[-(duese_upstream_lippe_laenge+upstream_puffer_laenge), duese_breite/2 + seite_puffer_breite, 0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge), duese_breite/2 + seite_puffer_breite, spalt_hoehe + upstream_puffer_hoehe],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge), 0, 0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge), 0, spalt_hoehe + upstream_puffer_hoehe]],
    [[0,1,3,2]]
    ); 
}}

module outlet_downstream() {
    color("Grey"){
    polyhedron(
    [[(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge, duese_breite/2 + seite_puffer_breite, 0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge, duese_breite/2 + seite_puffer_breite, spalt_hoehe + downstream_puffer_hoehe],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge, 0, 0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge, 0, spalt_hoehe + downstream_puffer_hoehe]],
    [[0,1,3,2]]
    ); 
}}

module symmetrie() {
    color("firebrick"){
    //Duese
    polyhedron(
    [[0,                    0, spalt_hoehe],
    [0,                     0, spalt_hoehe+duese_hoehe],
    [duese_schlitzweite,    0, spalt_hoehe],
    [duese_schlitzweite,    0, spalt_hoehe+duese_hoehe]],
    [[0,1,3,2]]
    ); 
        
    //Spalt
    polyhedron(
    [[-duese_upstream_lippe_laenge, 0, 0],
    [duese_schlitzweite + duese_downstream_lippe_laenge,   0, 0],
    [-duese_upstream_lippe_laenge, 0, spalt_hoehe],
    [duese_schlitzweite + duese_downstream_lippe_laenge,   0, spalt_hoehe]],
    [[0,1,3,2]]
    ); 
        
    //Upstream Puffer
    polyhedron(
    [[-duese_upstream_lippe_laenge, 0, 0],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),   0, 0],
    [-duese_upstream_lippe_laenge, 0, spalt_hoehe+upstream_puffer_hoehe],
    [-(duese_upstream_lippe_laenge+upstream_puffer_laenge),   0, spalt_hoehe+upstream_puffer_hoehe]],
    [[0,1,3,2]]
    ); 
    
    //Downstream Puffer + Auslauf
    polyhedron(
    [[duese_downstream_lippe_laenge+duese_schlitzweite, 0, 0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge,   0, 0],
    [(duese_schlitzweite+ausdehnung_downstream)+downstream_auslauf_laenge, 0, spalt_hoehe+downstream_puffer_hoehe],
    [(duese_downstream_lippe_laenge+duese_schlitzweite),   0, spalt_hoehe+downstream_puffer_hoehe]],
    [[0,1,2,3]]
    );
}}




