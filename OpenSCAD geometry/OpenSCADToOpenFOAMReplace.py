#OpenSCAD Konsoleninhalt wird auf OpenFOAM Syntax umgewandelt durch Search and Replace
#Kopiere Konsoleninhalt mit "ECHO" und speichere in Datei
#Führe dieses Skript mit der Datei als Argument aus (python OpenSCADToOpenFOAMReplace.py dateinameAusOpenSCAD)
#Skript erstellt neue Datei, die BlockMesh, SnappyHexMesh, setFields, Geschwindigkeit, etc. enthält

import sys
print("File: " + str(sys.argv[1]))

replacementStrings = list([",", "ECHO", ":", '"'])
filein = sys.argv[1]
fileout = filein + "_mod"

f = open(filein,'r')
filedata = f.read()
f.close()
newdata = filedata
for string in replacementStrings:
    newdata = newdata.replace(string, "")
    print(string)

f = open(fileout,'w')
f.write(newdata)
f.close()