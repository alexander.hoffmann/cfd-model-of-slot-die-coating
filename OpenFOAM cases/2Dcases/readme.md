# 2D Case

- Velocity boundary condition either localSlipSlotDieCoating or noSlip (0.org/U)
- dynamic mesh refinement: DynamicAMR implementation is necessary
  - if not needed, exclude by deleting constant/dynamicMeshDict
- Mesh generation with system/blockMeshDict
- TopoSet: If multiple processors are used for localSlipSlotDieCoating, then one processor must hold all adjacent cells on the boundary wall
  - usually not necessary, since localSlipSlotDieCoating is used in 2D and therefore the number of cells is small and one processor is sufficient

