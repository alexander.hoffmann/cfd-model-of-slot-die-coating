#!/bin/bash
 
# CLUSTER SETTINGS
#SBATCH --partition=multiple
#SBATCH --nodes=12
#SBATCH --ntasks-per-node=40
#SBATCH --time=72:00:00
#SBATCH --mem=70000mb
#SBATCH --mail-type=ALL
#SBATCH --mail-user=abc@example.com
 
module load cae/openfoam/v2012
source $FOAM_INIT
#foamInit

casepath=$(pwd)
echo "casepath"
echo $casepath

echo "compile custom code"
cd ${HOME}/OpenFOAM/dl8973-v2012/src/dynamicAMR
./Allwmake
export FOAM_USER_LIBBIN=/tmp/dl8973_${1}/OpenFOAM/dl8973-v2012/platforms/linux64GccDPInt32Opt/lib

echo "distribute to all nodes"
pdsh -w $SLURM_JOB_NODELIST mkdir -p /tmp/dl8973_${1}/OpenFOAM/dl8973-v2012/platforms/linux64GccDPInt32Opt/lib
sbcast ${HOME}/OpenFOAM/dl8973-v2012/platforms/linux64GccDPInt32Opt/lib/libdynamicLoadBalanceFvMesh.so /tmp/dl8973_${1}/OpenFOAM/dl8973-v2012/platforms/linux64GccDPInt32Opt/lib/libdynamicLoadBalanceFvMesh.so
echo "distribution done ----"

echo "---------------------end compile"

echo "---------------------------------"
echo "dynamicAMR libbin"
echo $FOAM_USER_LIBBIN
echo ""
echo "pwd"
echo $(pwd)

cd $casepath
echo "cd original pfad, pwd:"
echo $(pwd)


echo "---------------start sim"
echo "delete mesh, 0"
rm -r 0
mkdir 0
rm -r constant/polyMesh
rm -r constant/extendedFeatureEdgeMesh
rm -r constant/triSurface/*.eMesh

echo "mesh..."
blockMesh >logBlockmesh
surfaceFeatureExtract >logSurfacefeatureextract
decomposeParHPC >logDecomposepar
echo "before SHM"
mpirun -npernode 40 snappyHexMesh -overwrite -parallel >logSHM
echo "SHM done"
reconstructParMesh -mergeTol 5e-6 -constant >logReconstructParMesh
rm -r processor*
rm -r 0
cp -r 0.org 0
cp 0/alpha.water.orig 0/alpha.water
checkMesh >logCheckMesh
echo "mesh done"

echo "setFields, topoSet"
setFields >logSetFields

echo "decompose"
decomposeParHPC >logdecomposeParSolver

echo "-----run Sim"
mpirun -npernode 40 interFoam -parallel  >logSolver

echo "-----sim done"


echo "reconstruct"
reconstructParMesh -mergeTol 5e-6 -latestTime -constant >logReconstructParMeshSolver
reconstructPar -latestTime >logReconstructParSolver

echo "done done"
