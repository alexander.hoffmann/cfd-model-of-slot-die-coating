echo "---------------start sim"
echo "delete mesh, 0"
rm -r 0
mkdir 0
rm -r constant/polyMesh
rm -r constant/extendedFeatureEdgeMesh
rm -r constant/triSurface/*.eMesh

echo "mesh..."
blockMesh >logBlockmesh
surfaceFeatureExtract >logSurfacefeatureextract
echo "before SHM"
snappyHexMesh -overwrite >logSHM
echo "SHM done"
rm -r 0
cp -r 0.org 0
cp 0/alpha.water.orig 0/alpha.water
checkMesh >logCheckMesh
echo "mesh done"

echo "setFields, topoSet"
setFields >logSetFields

echo "-----run Sim"
interFoam -dry-run >logSolver

echo "-----sim done"
